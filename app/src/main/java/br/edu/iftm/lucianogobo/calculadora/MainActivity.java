package br.edu.iftm.lucianogobo.calculadora;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private TextView txtResultado;
    private TextView txtMemCalculo;
    private int numero = 0;
    private int numero2 = 0;
    private int sinalM = 0;
    private int pontoC1 = 0;
    private int pontoC2 = 0;
    private int redondo;
    private double numeroC;
    private double numero2C;
    private double Resultado;
    private String ResultadoS;
    private String ResultadoSM="";
    private String sinal;
    private String Snumero1 = "";
    private String Snumero2 = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.txtResultado = findViewById(R.id.txtResultado);
        this.txtMemCalculo = findViewById(R.id.txt_MemCaulculo);
    }

    public void onClickChangeText1(View view) {
        if (this.sinalM == 0 || sinalM == 1) {
            if (this.numero == 0) {
                this.Snumero1 = Snumero1 + "1";
                this.Snumero1 = tiraZero(Snumero1);
                this.txtResultado.setText(Snumero1);
            } else {
                this.Snumero2 = Snumero2 + "1";
                this.Snumero2 = tiraZero(Snumero2);
                this.txtResultado.setText(Snumero2);
            }
        }
    }

    public void onClickChangeText2(View view) {
        if (this.sinalM == 0 || sinalM == 1) {
            if (this.numero == 0) {
                this.Snumero1 = Snumero1 + "2";
                this.Snumero1 = tiraZero(Snumero1);
                this.txtResultado.setText(Snumero1);
            } else {
                this.Snumero2 = Snumero2 + "2";
                this.Snumero2 = tiraZero(Snumero2);
                this.txtResultado.setText(Snumero2);
            }
        }
    }

    public void onClickChangeText3(View view) {
        if (this.sinalM == 0 || sinalM == 1) {
             if (this.numero == 0) {
                 this.Snumero1 = Snumero1 + "3";
                 this.Snumero1 = tiraZero(Snumero1);
                 this.txtResultado.setText(Snumero1);
            } else {
                this.Snumero2 = Snumero2 + "3";
                 this.Snumero2 = tiraZero(Snumero2);
                this.txtResultado.setText(Snumero2);
            }
        }
    }

    public void onClickChangeText4(View view) {
        if (this.sinalM == 0 || sinalM == 1) {

            if (this.numero == 0) {
                this.Snumero1 = Snumero1 + "4";
                this.Snumero1 = tiraZero(Snumero1);
                this.txtResultado.setText(Snumero1);
            } else {
                this.Snumero2 = Snumero2 + "4";
                this.Snumero2 = tiraZero(Snumero2);
                this.txtResultado.setText(Snumero2);
            }
        }
    }

    public void onClickChangeText5(View view) {
        if (this.sinalM == 0 || sinalM == 1) {
            if (this.numero == 0) {
                this.Snumero1 = Snumero1 + "5";
                this.Snumero1 = tiraZero(Snumero1);
                this.txtResultado.setText(Snumero1);
            } else {
                this.Snumero2 = Snumero2 + "5";
                this.Snumero2 = tiraZero(Snumero2);
                this.txtResultado.setText(Snumero2);
            }
        }
    }

    public void onClickChangeText6(View view) {
        if (this.sinalM == 0 || sinalM == 1) {
            if (this.numero == 0) {
                this.Snumero1 = Snumero1 + "6";
                this.Snumero1 = tiraZero(Snumero1);
                this.txtResultado.setText(Snumero1);
            } else {
                this.Snumero2 = Snumero2 + "6";
                this.Snumero2 = tiraZero(Snumero2);
                this.txtResultado.setText(Snumero2);
            }
        }
    }

    public void onClickChangeText7(View view) {
        if (this.sinalM == 0 || sinalM == 1) {
            if (this.numero == 0) {
                this.Snumero1 = Snumero1 + "7";
                this.Snumero1 = tiraZero(Snumero1);
                this.txtResultado.setText(Snumero1);
            } else {
                this.Snumero2 = Snumero2 + "7";
                this.Snumero2 = tiraZero(Snumero2);
                this.txtResultado.setText(Snumero2);
            }
        }
    }

    public void onClickChangeText8(View view) {
        if (this.sinalM == 0 || sinalM == 1) {
            if (this.numero == 0) {
                this.Snumero1 = Snumero1 + "8";
                this.Snumero1 = tiraZero(Snumero1);
                this.txtResultado.setText(Snumero1);
            } else {
                this.Snumero2 = Snumero2 + "8";
                this.Snumero2 = tiraZero(Snumero2);
                this.txtResultado.setText(Snumero2);
            }
        }
    }

    public void onClickChangeText9(View view) {
        if (this.sinalM == 0 || sinalM == 1) {
            if (this.numero == 0) {
                this.Snumero1 = Snumero1 + "9";
                this.Snumero1 = tiraZero(Snumero1);
                this.txtResultado.setText(Snumero1);
            } else {
                this.Snumero2 = Snumero2 + "9";
                this.Snumero2 = tiraZero(Snumero2);
                this.txtResultado.setText(Snumero2);
            }
        }
    }

    public void onClickChangeText0(View view) {
        if (this.sinalM == 0 || sinalM == 1) {
            if (this.numero == 0) {
                this.Snumero1 = Snumero1 + "0";
                this.txtResultado.setText(Snumero1);
            } else {
                this.Snumero2 = Snumero2 + "0";
                this.txtResultado.setText(Snumero2);
            }
        }
    }

    public void onClickChangeTextMais(View view) {
        if (!this.Snumero1.equals("") && this.Snumero2.equals("")) {
            numero = 1;
            this.sinal = "+";
            this.txtResultado.setText("");
            this.sinalM = 1;
        }
        if (!this.Snumero1.equals("") && !this.Snumero2.equals("")) {
            this.numero2 = 1;
            this.sinalM = 1;
            resultadoFinal();
        }
    }

    public void onClickChangeTextMenos(View view) {

        if (!this.Snumero1.equals("") && this.Snumero2.equals("")) {
            numero = 1;
            this.sinal = "-";
            this.txtResultado.setText("");
            this.sinalM = 1;
        }else{
            if(this.Snumero1.equals("") && this.Snumero2.equals("")){
                this.Snumero1 = this.Snumero1 + "-";
            }
        }
        if (!this.Snumero1.equals("") && !this.Snumero2.equals("")) {
            numero2 = 1;
            resultadoFinal();
        }
    }

    public void onClickChangeTextMulti(View view) {
        if (!this.Snumero1.equals("") && this.Snumero2.equals("")) {
            numero = 1;
            this.sinal = "X";
            this.txtResultado.setText("");
            this.sinalM = 1;

        }
        if (!this.Snumero1.equals("") && !this.Snumero2.equals("")) {
            numero2 = 1;
            resultadoFinal();
        }
    }

    public void onClickChangeTextDivi(View view) {
        if (!this.Snumero1.equals("") && this.Snumero2.equals("")) {
            numero = 1;
            this.sinal = "/";
            this.txtResultado.setText("");
            this.sinalM = 1;

        }
        if (!this.Snumero1.equals("") && !this.Snumero2.equals("")) {
            numero2 = 1;
            resultadoFinal();
        }
    }

    public void onClickChangeTextIgual(View view) {
        if(!this.Snumero1.equals("") && this.Snumero2.equals("")){
     //       this.ResultadoSM = this.ResultadoSM + this.Snumero1 + "=" + this.Snumero1 + "\n";
            this.ResultadoSM = this.Snumero1 + "=" + this.Snumero1 + "\n" + this.ResultadoSM ;
            this.txtMemCalculo.setText(this.ResultadoSM);
            this.txtResultado.setText("");
            this.numero = 0;
            this.numero2 = 0;
            this.sinalM = 0;
            this.Snumero1="";
            this.Snumero2="";
            this.pontoC1=0;
            this.pontoC2=0;
        }
        if (!this.Snumero1.equals("") && !this.Snumero2.equals("")) {
            numero2 = 1;
            resultadoFinal();
        }
    }

    public void onClickChangeTextPonto(View view) {
        if ((this.Snumero1.equals("") || !this.Snumero1.equals("") ) && this.Snumero2.equals("")){
            this.pontoC1=this.pontoC1+1;
            if(this.pontoC1<2){
                this.Snumero1 = Snumero1 + ".";
            }
            this.txtResultado.setText(Snumero1);
        }
        if(this.sinalM==1){
            this.pontoC2=this.pontoC2+1;
            if(this.pontoC2<2){
                this.Snumero2 = Snumero2 + ".";
            }
            this.txtResultado.setText(Snumero2);
        }
        //this.sinal = ".";
    }

    public void onClickChangeTextC(View view) {
        this.txtResultado.setText(R.string.txt_0);
        //this.sinal = "C";
        this.numero = 0;
        this.numero2 = 0;
        this.sinalM = 0;
        this.Snumero1="";
        this.Snumero2="";
        this.pontoC1=0;
        this.pontoC2=0;
    }

    public void resultadoFinal() {
        this.numeroC = Double.parseDouble(Snumero1);
        this.numero2C = Double.parseDouble(Snumero2);
        if (this.sinal.equals("+")) {
            this.Resultado = numeroC + numero2C;
        }
        if (this.sinal.equals("-")) {
            this.Resultado = numeroC - numero2C;
        }
        if (this.sinal.equals("X")) {
            this.Resultado = numeroC * numero2C;
        }
        if (this.sinal.equals("/")) {
            if(this.numero2C==0.0){
                this.txtResultado.setText("Nao e possível dividir por zero!");
                this.numero = 0;
                this.numero2 = 0;
                this.sinalM = 0;
                this.Snumero1="";
                this.Snumero2="";
                this.pontoC1=0;
                this.pontoC2=0;
                return;
            }else{
                this.Resultado = numeroC / numero2C;
            }
        }
        if(this.Resultado-(int)this.Resultado!=0.0){
            this.ResultadoS = String.valueOf(this.Resultado);
        }else{
            this.redondo=(int)this.Resultado;
            this.ResultadoS = String.valueOf(redondo);
        }
 //       this.ResultadoSM = this.ResultadoSM + Snumero1 + sinal + Snumero2 + "=" +
 //               this.ResultadoS+ "\n";
        this.ResultadoSM = Snumero1 + sinal + Snumero2 + "=" +
                this.ResultadoS+ "\n"+ this.ResultadoSM;
        this.txtResultado.setText("");
        this.txtResultado.setText(this.ResultadoS);
        this.txtMemCalculo.setText(this.ResultadoSM);
        this.Snumero1 = this.ResultadoS;
        this.Snumero2 = "";
        this.sinalM = 2;
        this.pontoC2=0;
    }

    public String tiraZero(String variavel){
        this.numeroC=Double.parseDouble(variavel);
        if(this.numeroC-(int)this.numeroC!=0.0){
            variavel = String.valueOf(this.numeroC);
        }else{
            this.redondo=(int)this.numeroC;
            variavel = String.valueOf(redondo);
        }
        return variavel;
    }


}